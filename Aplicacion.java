package AI_4;

public class Aplicacion implements IHM {
    public static void main(String[] args) {
        Aplicacion app = new Aplicacion();
        app.Run();
    }

    /**
     * Para el segundo ejemplo:
     * Tasa de crossover
     */
    public void Run() {
        //resolución del viajante de comercio
        //argumentos
        Argumentos.tasaCrossover = 0.6;
        Argumentos.tasaMutacion = 0.5;
        Argumentos.tasaAdicionGenes = 0.9;
        Argumentos.tasaElimGen = 0.1;
        Argumentos.minFitness = 0;
        Argumentos.numMaxGeneraciones = 20;

        //ejecución
        ProcesoEvolutivo sist = new ProcesoEvolutivo(this, "Lab");
        sist.Run();
    }

    /**
     * 
     */
    @Override
    public void MostrarMejorIndividuo(Individuo ind, int generacion) {
        System.out.println(generacion + " -> " + ind.toString());
        
    }

    
}
