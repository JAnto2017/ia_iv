# ALGORITMOS GENÉTICOS

Basados en la evolución biológica.

## Primeras fases del algoritmo

Será necesario hacer opciones sobre las representaciones y después, inicializar los individuos de la población inicial.

Será necesario seleccionar su función de evaluación.

## Elección de las representaciones

La elección de las representaciones es primordial para limitar el espacio de búsqueda y hacerlo lo más adaptado posible al algoritmo elegido.

### Población de individuos

La _población_ contiene una lista de individuos.

Loa _individuos_ contienen una lista de genes.

### Genes

La representación de los genes es aquella sobre la que hay que pasar la mayor parte del tiempo.
Se trata de una _lista ordenada_ de valores.

Se desaconseja tener demasiados valores a optimizar y evitar así variables redundantes.

Para un algoritmo genético, cuanto más desacoplados estén los genes, más fácil es converger hacia la respuesta correcta.

## Inicialización de la población inicial

Durante la fase de inicialización, se crea una primera población.

Si ya conocemos soluciones aceptables al problema, es posible inyectarlas directamente durante la inicialización.

## Reproducción

Durante la reproducción, se elige para cada hijo desde 1 hasta N padres.

### Crossover

El ***crossover***, permite crear un nuevo descendiente a partir de sus dos ascendentes, mezclando la información genética. Es necesario determinar la ***tasa de crossover*** del algoritmo, normalmente superior al 50%.

### Mutación

El segundo operador local es el operador de ***mutación***.
Su objetivo es introducir novedades dentro de la población, para permitir descubrir nuevas soluciones potenciales.

La mutación puede seguir una ***distribución uniforme*** centrada en 0 y que permite obtener la modificación a aplicar a la versión actual.

# Dominios de aplicación

Los algoritmos evolutivos, para que sean eficaces:
+ El número de soluciones potenciales debe ser muy grande.
+ No hay método exacto que permite obtener una solución.
+ Una solución casi óptima es aceptable.
+ Se puede evaluar la calidad de una solución potencial.

# Implementación

Implementar un algoritmo genético y genérico en Java.
Los diferentes argumentos del algoritmo se definen en una clase aparte.
Interfaces y clases abstractas se crean para los individuos y los genes.

Para el problema de salida del laberinto, necesitamos tener genomas de tamaño variable, por lo que el algoritmo debe permitir gestionarlo.

Definimos una interfaz para el programa principal. La salida será por consola.

## Solución del programa parte I

En la siguiente imagen se observa la solución con los parámetros de entrada siguientes:

* Argumentos.tasaCrossover = 0.0;
* Argumentos.tasaMutacion = 0.3;
* Argumentos.tasaAdicionGenes = 0.0;
* Argumentos.tasaElimGen = 0.0;
* Argumentos.minFitness = 2579;

![Solución ejemplo 1](AI_4_sol_1.png)

## Solución del programa parte II

Los argumentos más adaptados a este problema parte II:

- Tasa de crossover del 60%.
- Mutaciones de tasa de 0.1 (un gen de cada 10 de media), la adición de un gen en el 80% de los casos y la eliminación en el 10% (de esta manera, hay tendencia a alargar las rutas en lugar de acortarlas)
- Fitness mínimo previsto es nulo, es decir que se llega a la casilla de salida.

A continuación se muestran los resultados:

![Solución ejemplo 1](AI_4_sol_2.png)

![Solución ejemplo 1](AI_4_sol_3.png)

# Resumen

> Los algoritmos genéticos o evolutivos, están inspirados en las diferentes búsquedas realizadas en biología de la evolución.

> Tenemos una población de individuos cada una compuesta por una genoma, que es una lista de genes. Estos individuos se evalúan respecto a la calidad de la solución de un problema dado, que representan.

> Los mejores individuos se seleccionan para ser reproducidos. Se crean nuevas soluciones a partir de uno o varios padres. En caso de que intervengan varios padres se realiza un crossover, es decir, un cruce entre la información genética de los diferentes padres.

> A continuación, los genomas de los descendientes sufren mutaciones aleatorias, que representan los errores de copiado que tiene lugar durante la reproducción. Cada descendiente, aunque sean parecidos a sus padres, es potencialmente diferente.

> Esta nueva generación debe sobrevivir para formar parte de la población de la generación siguiente. Volvemos a ejecutar el proceso hasta alcanzar las soluciones óptimas o casi óptimas.

> De esta manera, los algoritmos genéticos permiten resolver muchos problemas para los que no existe solución matemática conocida y cuyo espacio de búsqueda es demasiado vasto para una búsqueda exhaustiva.