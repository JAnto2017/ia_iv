package AI_4.PVC;

import java.util.ArrayList;

import AI_4.Argumentos;
import AI_4.IGen;
import AI_4.Individuo;

public class PVCIndividuo extends Individuo {
    public PVCIndividuo() {
        genoma = new ArrayList<>();
        ArrayList<Integer> indiceDispo = PVC.getCiudadIndice();

        while (!indiceDispo.isEmpty()) {
            int indices = Argumentos.random.nextInt(indiceDispo.size());
            genoma.add(new PVCGen(indiceDispo.get(indices)));
            indiceDispo.remove(indices);
        }
    }

    /**
     * 
     * @param padre1
     * @param padre2
     */
    public PVCIndividuo(PVCIndividuo padre1, PVCIndividuo padre2) {
        genoma = new ArrayList<>();
        int ptCorte = Argumentos.random.nextInt(padre1.genoma.size());

        for (int i = 0; i < ptCorte; i++) {
            genoma.add(new PVCGen((PVCGen) padre1.genoma.get(i)));
        }

        for (IGen g : padre2.genoma) {
            if (!genoma.contains((PVCGen)g)) {
                genoma.add(new PVCGen((PVCGen)g));
            }
        }
        Mutar();
    }

    /**
     * 
     * @param padre
     */
    public PVCIndividuo(PVCIndividuo padre) {
        genoma = new ArrayList<>();
        for (IGen g : padre.genoma) {
            this.genoma.add(new PVCGen((PVCGen)g));
        }
        Mutar();
    }


    /**
     * 
     */
    @Override
    public void Mutar() {
        if (Argumentos.random.nextDouble() < Argumentos.tasaMutacion) {
            int indice1 = Argumentos.random.nextInt(genoma.size());
            PVCGen g = (PVCGen) genoma.get(indice1);
            genoma.remove(g);
            int indice2 = Argumentos.random.nextInt(genoma.size());
            genoma.add(indice2,g);
        }        
    }

    /**
     * 
     */
    @Override
    public double Evaluar() {
        int kmTotal = 0;
        PVCGen antiguoGen = null;

        for (IGen g : genoma) {
            if (antiguoGen != null) {
                kmTotal += ((PVCGen)g).getDistancia(antiguoGen);
            }
            antiguoGen = (PVCGen)g;
        }
        kmTotal += antiguoGen.getDistancia((PVCGen)genoma.get(0));
        Fitness = kmTotal;
        return Fitness;
    }
    /**
     * 
     * @param tipo
     * @param padre
     * @return
     */
    public Individuo CrearIndividuo(String tipo,Individuo padre) {
        Individuo ind = null;
        switch (tipo) {
            case "PVC":
                ind = new PVCIndividuo((PVCIndividuo)padre);
                break;
        }
        return ind;
    }
    /**
     * 
     * @param tipo
     * @param padre1
     * @param padre2
     * @return
     */
    public Individuo crearIndividuo(String tipo,Individuo padre1,Individuo padre2) {
        Individuo ind = null;
        switch (tipo) {
            case "PVC":
                ind = new PVCIndividuo((PVCIndividuo)padre1,(PVCIndividuo)padre2);
                break;                    
        }
        return ind;
    }
}
