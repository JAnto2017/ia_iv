package AI_4.PVC;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Problema del Viajante de Comercio
 * Contiene lista de ciudades y una tabla de doble entrada que indica las distancias que separan estas ciudades.
 *  
 */
public class PVC {
   static ArrayList<String> ciudades;
   static int[][] distancias;

   //métodos
   public static void Init() {
    ciudades = new ArrayList<>(Arrays.asList("Paris","Lyon","Marsella","Nantes","Burdeos","Toulouse","Lille"));

    distancias = new int[ciudades.size()][];
    distancias[0] = new int[] {20,462,772,379,546,678,215};
    distancias[1] = new int[] {462,10,326,598,842,506,664};
    distancias[2] = new int[] {772,326,10,909,555,407,105};   
    distancias[3] = new int[] {379,598,909,10,338,540,584};
    distancias[4] = new int[] {546,842,555,338,10,250,792};
    distancias[5] = new int[] {678,506,407,540,250,10,926};
    distancias[6] = new int[] {215,664,105,584,792,926,0};
    
   }

   protected static int getDistancia(int ciudad1,int ciudad2) {
    return distancias[ciudad1][ciudad2];
   }

   protected static String getCiudad(int ciudadIndice) {
    return ciudades.get(ciudadIndice);
   }

   protected static ArrayList<Integer> getCiudadIndice() {
    int numCiudades = ciudades.size();
    ArrayList<Integer> ciudadesIndex = new ArrayList<>();
    for (int i = 0; i < numCiudades; i++) {
        ciudadesIndex.add(i);
    }
    return ciudadesIndex;
   }
}
