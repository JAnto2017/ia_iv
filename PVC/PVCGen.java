package AI_4.PVC;

import AI_4.IGen;

public class PVCGen implements IGen{
    protected int ciudadIndice;

    //métodos
    /**
     * --------------------------------------------------------------
     * @param _ciudadIndice
     */
    public PVCGen(int _ciudadIndice){
        ciudadIndice = _ciudadIndice;
    }

    /**
     * --------------------------------------------------------------
     * @param g
     */
    public PVCGen(PVCGen g){
        ciudadIndice = g.ciudadIndice;
    }

    /**
     * --------------------------------------------------------------
     * @param g
     * @return
     */
    protected int getDistancia(PVCGen g){
        return PVC.getDistancia(ciudadIndice, g.ciudadIndice);
    }

    /**
     * --------------------------------------------------------------
     */
    @Override
    public void Mutar() {        
        
    }

    /**
     * --------------------------------------------------------------
     */
    @Override
    public String toString() {
        return PVC.getCiudad(ciudadIndice);
    }
}
