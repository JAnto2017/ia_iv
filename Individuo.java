package AI_4;

import java.util.ArrayList;
import java.util.StringJoiner;

/**
 * Código necesario para recuperar el Fitness de un individuo o su genoma.
 * 
 */
public abstract class Individuo {
    protected double Fitness;
    protected ArrayList<IGen> genoma;

    public double getFitness() {
        return Fitness;
    }

    public abstract void Mutar();
    public abstract double Evaluar();

    @Override
    public String toString() {
        String gen = "(" + Fitness + ")";
        StringJoiner sj = new StringJoiner(" - ");
        sj.add(gen);

        for (IGen g : genoma) {
            sj.add(g.toString());
        }
        return sj.toString();
    }
}
