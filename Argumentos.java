package AI_4;

import java.util.Random;

/**
 * Definimos los argumentos relativos a toda la población y al algoritmo en general.
 * El nº de individuos por generación, numIndividuos, se inicializa a 20.
 * El nº máx de generación, numMaxGeneraciones, se inicializará a 50.
 * El nº inicial de genes si el genoma es de tamaño variable, numGenes, se inicializa a 10.
 * El Fitness que se debe alcanzar, minFitness, se inicializa a 0.
 * 
 * Se definen diferentes tasas utilizadas durante la reproducción: tasa de mutaciones a 0.1; tasa de adición de genes a 0.2; tasa de eliminación de genes a 0.1; tasa de crossover a 0.6
 */

public class Argumentos {
    //argumentos de la población y los individuos
    public static int numIndividuos = 20;
    public static int numGenes = 10;
    
    //criterios de parada
    public static int numMaxGeneraciones = 50;
    public static double minFitness = 0.0;

    //tasa de las operaciones
    public static double tasaMutacion = 0.1;
    public static double tasaAdicionGenes = 0.2;
    public static double tasaElimGen = 0.1;
    public static double tasaCrossover = 0.6;

    //generador aleatorio
    public static Random random = new Random();
}
