package AI_4;

/**
 * Sólo contiene una función de mutación.
 */
public interface IGen {
    void Mutar();
}
