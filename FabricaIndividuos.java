package AI_4;

import AI_4.PVC.PVC;
import AI_4.PVC.PVCIndividuo;
import AI_4.laberinto.LabIndividuo;
import AI_4.laberinto.Laberinto;

public class FabricaIndividuos {
    
    private static FabricaIndividuos instance;
    private FabricaIndividuos() {}

    public static FabricaIndividuos getInstance() {
        if (instance == null) {
            instance = new FabricaIndividuos();
        }
        return instance;
    }

    /**
     * Inicializar el entorno de vida de los individuos.
     * Obtener el individuo deseado creado de manera aleatoria o a partir de uno o dos padres gracias a tres métodos, que toman como argumentos el problema a resolver en forma de una cadena y después, los padres potenciales.
     * 
     * @param tipo
     */
    void Init (String tipo) {
        switch (tipo) {
            case "PVC":
                PVC.Init();
                break;
            case "Lab":
                Laberinto.Init(Laberinto.Lab2);
                break;
        }
    }

    public Individuo CrearIndividuo(String tipo) {
        Individuo ind = null;
        switch (tipo) {
            case "PVC":
                ind = new PVCIndividuo();
                break;
            case "Lab":
                ind = new LabIndividuo();
                break;
        }
        return ind;
    }

    public Individuo CrearIndividuo(String tipo, Individuo padre) {
        Individuo ind = null;
        
        switch (tipo) {
            case "PVC":
                ind = new PVCIndividuo((PVCIndividuo)padre);
                break;
            case "Lab":
                ind = new LabIndividuo((LabIndividuo)padre);
                break;
        }
        return ind;
    }

    public Individuo CrearIndividuo(String tipo,Individuo padre1,Individuo padre2) {
        Individuo ind = null;
        
        switch (tipo) {
            case "PVC":
                ind = new PVCIndividuo((PVCIndividuo)padre1,(PVCIndividuo)padre2);
                break;
        
            case "Lab":
                ind = new LabIndividuo((LabIndividuo)padre1,(LabIndividuo)padre2);
                break;
        }
        return ind;
    }

    
}
