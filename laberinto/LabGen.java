package AI_4.laberinto;

import AI_4.Argumentos;
import AI_4.IGen;

/**
 * Un gen contiene únicamente una dirección a seguir.
 * Un constructor para crear un gen de manera aleatoria y otro para copiar un gen dado como argumento.
 * 
 */
public class LabGen implements IGen{
    public Laberinto.Direccion direccion;

    public LabGen() {
        direccion = Laberinto.Direccion.values()[Argumentos.random.nextInt(4)];
    }

    public LabGen(LabGen g) {
        direccion = g.direccion;
    }

    @Override
    public String toString() {
        return direccion.name().substring(0,1);
    }

    /**
     * Es suficiente con volver a hacer una selección al azar para una nueva dirección.
     * Como hay cuatro direcciones posible, en un 25% de los casos nos encontraremos con la dirección que teníamos antes de la mutación.
     * 
     */
    @Override
    public void Mutar() {
        direccion = Laberinto.Direccion.values()[Argumentos.random.nextInt(4)];        
    }
}
