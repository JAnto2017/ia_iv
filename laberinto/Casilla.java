package AI_4.laberinto;

public class Casilla {
    public int i;
    public int j;

    public Casilla(int _i, int _j) {
        i = _i;
        j = _j;
    }

    @Override
    public boolean equals(Object obj) {
        return (i == ((Casilla)obj).i && j == ((Casilla)obj).j);
    }
}
