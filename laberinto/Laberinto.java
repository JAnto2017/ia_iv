package AI_4.laberinto;

import java.util.ArrayList;

import org.junit.platform.console.shadow.picocli.CommandLine.Model.IGetter;

import AI_4.IGen;

public class Laberinto {
    private static ArrayList<Casilla[]> rutas;
    private static Casilla entrada;
    private static Casilla salida;

    //otros atributos

    public static String Lab1 = "*--*--*--*--*\n" +
                                "E           |\n" +
                                "*  *  *--*--*\n" +
                                "|  |  |     |\n" +
                                "*  *--*     *\n" +
                                "|           |\n" +
                                "*  *--*--*  *\n" +
                                "|        |  S\n" +
                                "*--*--*--*--*";

    public static String Lab2 = "*--*--*--*--*--*--*\n" +
                                "E           |     |\n" +
                                "*--*--*     *  *--*\n" +
                                "|     |     |     |\n" +
                                "*  *  *     *     *\n" +
                                "|  |  |           |\n" +
                                "*--*  *  *--*--*  *\n" +
                                "|        |     |  |\n" +
                                "*  *--*--*  *  *  *\n" +
                                "|  |        |  |  |\n" +
                                "*  *  *  *--*  *  *\n" +
                                "|     |           S\n" +
                                "*--*--*--*--*--*--*";
    
    public enum Direccion {Arriba, Abajo, Izquierda, Derecha};

    //métodos
    public static void Init(String s) {
        rutas = new ArrayList<>();
        String[] lineas = s.split("\n");
        int numLineas = 0;

        for (String linea : lineas) {
            if (numLineas%2 != 0) {
                //num impar, por lo tanto linea de pasillo
                int indices = linea.indexOf("E");
                if (indices != -1) {
                    //tenemos una entrada en este pasillo
                    if (indices == linea.length() - 1) {
                        indices--;
                    }
                    entrada = new Casilla(numLineas/2, indices/3);
                }
                indices = linea.indexOf("S");
                if (indices != -1) {
                    //tenemos una salida en el pasillo
                    if (indices == linea.length() - 1) {
                        indices--;
                    }
                    salida = new Casilla(numLineas/2, indices/3);
                }
                //recorremos el pasillo para crear las rutas horizontales
                for (int columna = 0; columna < lineas.length /3; columna++) {
                    String casillaStr = linea.substring(columna*3, columna*3 + 3);
                    if (!casillaStr.contains("|") && !casillaStr.contains("S")) {
                        //ruta abiera se añade
                        rutas.add(new Casilla[]{new Casilla(numLineas/2, columna-1), new Casilla(numLineas/2, columna)});
                    }
                }
            } else {
                //línea par: estamos en los muros
                String[] casillas = linea.substring(1).split("\\*");
                int columna = 0;
                for (String bloque : casillas) {
                    if (bloque.equals(" ")) {
                        //ruta abierta, se añade
                        rutas.add(new Casilla[] {new Casilla(numLineas/2 - 1, columna), new Casilla(numLineas/2, columna)});
                    }
                    columna++;
                }
            }
            numLineas++;
        }
    }

    /**
     * Permite determinar si es posible ir desde una casilla hasta otra.
     * Las rutas solo se guardan una vez, aunque se toman en los dos sentidos.
     * @param pos1
     * @param pos2
     * @return
     */
    private static boolean esPosible(Casilla pos1, Casilla pos2) {
        for (Casilla[] camino : rutas) {
            if ((camino[0].equals(pos1) && camino[1].equals(pos2)) || ((camino[0].equals(pos2) && camino[1].equals(pos1)))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Permite saber si una casilla es un cruce. Para ello, se cuenta el número de rutas que llegan hasta la casilla.
     * Si hay tres o más, entonces es un cruce.
     * @param pos
     * @return
     */
    private static boolean esCruce(Casilla pos) {
        int numCaminos = 0;
        for (Casilla[] camino : rutas) {
            if (camino[0].equals(pos) || camino[1].equals(pos)) {
                numCaminos++;
            }
        }
        return numCaminos > 2;
    }

    public static void Mover(Casilla inicio,int movI, int movJ) {
        boolean finMovimiento = false;

        while (esPosible(inicio, new Casilla(inicio.i + movI, inicio.j + movJ))) {
            inicio.i += movI;
            inicio.j += movJ;
            finMovimiento = esCruce(inicio) || inicio.equals(salida);
        }
    }

    /**
     * 
     * @param genoma
     * @return
     */
    public static double Evaluar(ArrayList<IGen> genoma) {
        Casilla casillaActual = new Casilla(entrada.i, entrada.j);
        boolean finMovimiento = false;

        for (IGen g : genoma) {
            switch (((LabGen)g).direccion) {
                case Abajo:
                    Mover(casillaActual, 1, 0);
                    break;
                case Arriba:
                    Mover(casillaActual, -1, 0);
                    break;
                case Derecha:
                    Mover(casillaActual, 0, 1);
                    break;
                case Izquierda:
                    Mover(casillaActual, 0, -1);
                    break;                
            }

            if (casillaActual.equals(salida)) {
                break;
            }
        }
        // cálculo Firness: distancia de Manhattan
        int distancia = Math.abs(salida.i - casillaActual.i) + Math.abs(salida.j - casillaActual.j);

        return distancia;
    }
}
