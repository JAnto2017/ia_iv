package AI_4.laberinto;

import java.util.ArrayList;

import AI_4.Argumentos;
import AI_4.IGen;
import AI_4.Individuo;

/**
 * Esta clase no tiene atributos.
 * Empezamos creando un primer constructor que no recibe argumentos y que por lo tanto permite crear individuos de manera aleatoria.
 * Esto tendrá tantos genes como se definen en la clase configurada.
 */
public class LabIndividuo extends Individuo{
    // constructor 1
    public  LabIndividuo() {
        genoma = new ArrayList<>();
        for (int i = 0; i < Argumentos.numGenes; i++) {
            genoma.add(new LabGen());
        }
    }
    // constructor 2
    public LabIndividuo(LabIndividuo padre) {
        genoma = new ArrayList<>();

        for (IGen g : padre.genoma) {
            genoma.add(new LabGen((LabGen) g));
        }
        Mutar();
    }
    // constructor 3
    public LabIndividuo(LabIndividuo padre1, LabIndividuo padre2) {
        genoma = new ArrayList<>();
        // crossover
        int indices = Argumentos.random.nextInt(padre1.genoma.size());

        for (IGen g : padre1.genoma.subList(0, indices)) {
            genoma.add(new LabGen((LabGen) g));
        }

        if (indices < padre2.genoma.size()) {
            for (IGen g : padre2.genoma.subList(indices, padre2.genoma.size())) {
                genoma.add(new LabGen((LabGen) g));
            }
        }
        // mutar
        Mutar();
    }

    /**
     * Permite mutar a nuestros individuos. Esta mutación puede tener 3 formas diferentes:
     * tasa definida por tasaElimGen.
     * tasa definida por tasaAdicionGenes.
     * tasa por gen de tasaMutación.
     * 
     */
    @Override
    public void Mutar() {
        // Eliminación de un gen?
        if (Argumentos.random.nextDouble() < Argumentos.tasaElimGen) {
            int indices = Argumentos.random.nextInt(genoma.size());
            genoma.remove(indices);
        }  
        // Añadir gen al final?
        if (Argumentos.random.nextDouble() < Argumentos.tasaAdicionGenes) {
            genoma.add(new LabGen());
        }      
        // Cambio de valores?
        for (IGen g : genoma) {
            if (Argumentos.random.nextDouble() < Argumentos.tasaMutacion) {
                g.Mutar();
            }
        }
    }

    /**
     * Implica mover al individuo en el laberinto.
     * Por lo tanto, empezamos llamando al método de evaluación de la clase Laberinto de la clase Individuo.
     * 
     */
    @Override
    public double Evaluar() {
        
        Fitness = Laberinto.Evaluar(genoma);
        return Fitness;
    }
}
