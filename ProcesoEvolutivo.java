package AI_4;

import java.util.ArrayList;

public class ProcesoEvolutivo {
    protected ArrayList<Individuo> poblacion;
    protected int numGeneracion = 0;
    protected IHM ihm = null;
    protected double mejorFitness;
    protected String problema;

    //constructor
    public ProcesoEvolutivo(IHM _ihm,String _problema) {
        ihm = _ihm;
        problema = _problema;
        FabricaIndividuos.getInstance().Init(problema);
        poblacion = new ArrayList<>();

        for (int i = 0; i < Argumentos.numIndividuos; i++) {
            poblacion.add(FabricaIndividuos.getInstance().CrearIndividuo(problema));
        }
    }

    /**
     * Gestiona la supervivencia de una generación a la siguiente.
     * Se elige una simple sustitución; en cada generación, todos los hijos se convierten en los adultos que van desapareciendo.
     * 
     * @param nuevaGeneracion
     */
    private void Supervivencia(ArrayList<Individuo> nelleGeneracion) {
        poblacion = nelleGeneracion;
    }

    /**
     * Selección de los individuos para convertirlos en padres.
     * Elegimos un torneo.
     * Para ello, en primer lugar, hay que seleccionar dos individuos, de manera aleatoria en la población.
     * A continuación, se compara y se guarda el que tenga el Fitness más pequeño, el más adaptado.
     * 
     * @return
     */
    private Individuo Seleccion() {
       int indice1 = Argumentos.random.nextInt(Argumentos.numIndividuos);
       int indice2 = Argumentos.random.nextInt(Argumentos.numIndividuos);

       if (poblacion.get(indice1).Fitness <= poblacion.get(indice2).Fitness) {
        return poblacion.get(indice1);
       } else {
        return poblacion.get(indice2);
       }
    }

    /**
     * Actúa en bucle hasta alcanzar el número máximo de generaciones o el Fitness destino fijado en los argumentos.
     * Utiliza dos métodos privado.
     * 
     */
    public void Run() {
        mejorFitness = Argumentos.minFitness + 1;

        while ((numGeneracion < Argumentos.numMaxGeneraciones) && (mejorFitness > Argumentos.minFitness)) {
            Individuo mejorInd = EvaluarYRecuperarMejorInd(poblacion);
            mejorFitness = mejorInd.Fitness;
            ArrayList<Individuo> nuevaPoblacion = Reproduccion(mejorInd);
            Supervivencia(nuevaPoblacion);
            numGeneracion++;
        }
    }

    //evalúa toda la población y devuelve el mejor individuo
    private Individuo EvaluarYRecuperarMejorInd(ArrayList<Individuo> poblacion) {
        Individuo mejorInd = poblacion.get(0);
        for (Individuo ind : poblacion) {
            ind.Evaluar();

            if (ind.Fitness < mejorInd.Fitness) {
                mejorInd = ind;
            }
        }
        ihm.MostrarMejorIndividuo(mejorInd, numGeneracion);
        return mejorInd;
    }

    //selección y reproducción con elitismo, crossover y mutación
    private ArrayList<Individuo> Reproduccion(Individuo mejorInd) {
        ArrayList<Individuo> nuevaPoblacion = new ArrayList<>();
        nuevaPoblacion.add(mejorInd);

        for (int i = 0; i < Argumentos.numIndividuos - 1; i++) {
            //con o sin crossover ?
            if (Argumentos.random.nextDouble() < Argumentos.tasaCrossover) {
                //con crossover, por lo tanto dos padres
                Individuo padre1 = Seleccion();
                Individuo padre2 = Seleccion();
                nuevaPoblacion.add(FabricaIndividuos.getInstance().CrearIndividuo(problema,padre1,padre2));
                
            } else {
                //sin crossover, un único padre
                Individuo padre = Seleccion();
                nuevaPoblacion.add(FabricaIndividuos.getInstance().CrearIndividuo(problema,padre));
            }
        }
        return nuevaPoblacion;
    }
}
